package com.muhardin.endy.training.swing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer {
    private AplikasiChat aplikasiChat;

    public ChatServer(AplikasiChat aplikasiChat) {
        this.aplikasiChat = aplikasiChat;
    }

    public void start(){
        aplikasiChat.tambahMessage("Server start di port "+aplikasiChat.getPort());

        new Thread() {
            public void run(){
                while(true) {
                    try (ServerSocket serverSocket = new ServerSocket(aplikasiChat.getPort())) {
                        System.out.println("Menunggu koneksi di port : "+aplikasiChat.getPort());
            
                        Socket clientSocket = serverSocket.accept();
                        
                        // anonymous inner class
                        new Thread(){
                            public void run(){
                                System.out.println("Terima koneksi dari IP "+clientSocket.getInetAddress().toString());
            
                                try (BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))){
                                    String data;
                                    while((data = in.readLine()) != null) {
                                        aplikasiChat.tambahMessage(clientSocket.getInetAddress().toString() + " > "+data);
                                        if("quit".equalsIgnoreCase(data)){
                                            break;
                                        }
                                    }
                                } catch (Exception err) {
                                    err.printStackTrace();
                                }
                            }
                        }.start();  
                        
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } 
            }
        }.start(); 
    }
}
