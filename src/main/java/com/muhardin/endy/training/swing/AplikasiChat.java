package com.muhardin.endy.training.swing;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.Buffer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AplikasiChat {
    private JFrame frame;
    private JTextArea txtChat;
    private JTextField txtPort;
    private JTextField txtIp;
    private JTextField txtPesan;
    private JTextField txtUsername;

    private PrintWriter chatOut;

    public AplikasiChat(){
        siapkanLayoutAplikasi();
    }

    public void start(){
        frame.setVisible(true);
    }

    public void tambahMessage(String msg){
        txtChat.append(msg + "\r\n");
    }

    public String bacaPesan(){
        return txtPesan.getText();
    }

    public Integer getPort(){
        return Integer.valueOf(txtPort.getText());
    }

    public void setChatOutput(PrintWriter output){
        this.chatOut = output;
    }

    public void kirimChat(){
        chatOut.println(txtUsername.getText() + ">" + txtPesan.getText());
        txtPesan.setText("");
    }

    public void connect(){
        try {
            Socket socket = new Socket(txtIp.getText(), getPort());
            chatOut = new PrintWriter(socket.getOutputStream(), true);
            chatOut.println("Connected ");
        } catch (Exception err){
            err.printStackTrace();
        }
    }

    private void siapkanLayoutAplikasi() {
        frame = new JFrame();
        frame.setTitle("Aplikasi Chat");
        frame.setSize(600, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        // textfield
        txtIp = new JTextField("127.0.0.1");
        txtPort = new JTextField("12345");
        txtUsername = new JTextField();
        txtPesan = new JTextField(30);

        // textarea
        txtChat = new JTextArea();
        txtChat.setLineWrap(true);

        // button
        JButton btnConnect = new JButton("Connect");
        JButton btnServer = new JButton("Server");
        JButton btnKirim = new JButton("Kirim");

        // action listener button
        btnServer.addActionListener(new TombolServerListener(this));
        btnConnect.addActionListener(new TombolConnectListener(this));
        btnKirim.addActionListener(new TombolKirimListener(this));

        // label
        JLabel lblUsername = new JLabel("Username");
        JLabel lblIp = new JLabel("IP");

        // layout panel atas
        JPanel panelAtas = new JPanel();
        panelAtas.setLayout(new GridBagLayout());

        GridBagConstraints c1 = new GridBagConstraints();
        c1.gridx = 0;
        c1.gridy = 0;
        panelAtas.add(lblIp, c1);

        GridBagConstraints c2 = new GridBagConstraints();
        c2.gridx = 1;
        c2.gridy = 0;
        c2.fill = GridBagConstraints.HORIZONTAL;
        c2.weightx = 0.5;
        panelAtas.add(txtIp, c2);

        GridBagConstraints c3 = new GridBagConstraints();
        c3.gridx = 2;
        c3.gridy = 0;
        panelAtas.add(new JLabel("Port"), c3);

        GridBagConstraints c4 = new GridBagConstraints();
        c4.gridx = 3;
        c4.gridy = 0;
        c4.weightx = 0.1;
        c4.fill = GridBagConstraints.HORIZONTAL;
        panelAtas.add(txtPort, c4);

        GridBagConstraints c5 = new GridBagConstraints();
        c5.gridx = 0;
        c5.gridy = 1;
        panelAtas.add(lblUsername, c5);

        GridBagConstraints c6 = new GridBagConstraints();
        c6.gridx = 1;
        c6.gridy = 1;
        c6.fill = GridBagConstraints.HORIZONTAL;
        panelAtas.add(txtUsername, c6);

        GridBagConstraints c7 = new GridBagConstraints();
        c7.gridx = 2;
        c7.gridy = 1;
        c7.fill = GridBagConstraints.HORIZONTAL;
        panelAtas.add(btnConnect, c7);

        GridBagConstraints c8 = new GridBagConstraints();
        c8.gridx = 3;
        c8.gridy = 1;
        c8.fill = GridBagConstraints.HORIZONTAL;
        panelAtas.add(btnServer, c8);

        frame.getContentPane().add(panelAtas, BorderLayout.PAGE_START);

        // layout panel bawah
        JPanel panelBawah = new JPanel();
        panelBawah.add(new JLabel("Pesan"));
        panelBawah.add(txtPesan);
        panelBawah.add(btnKirim);
        frame.getContentPane().add(panelBawah, BorderLayout.PAGE_END);

        JScrollPane scrollPane = new JScrollPane(txtChat);
        frame.getContentPane().add(scrollPane,BorderLayout.CENTER);
    }

    class TombolServerListener implements ActionListener{

        private AplikasiChat aplikasiChat;

        public TombolServerListener(AplikasiChat aplikasiChat){
            this.aplikasiChat = aplikasiChat;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ChatServer server = new ChatServer(aplikasiChat);
            server.start();
        }

    }

    class TombolConnectListener implements ActionListener{
        private AplikasiChat aplikasiChat;

        public TombolConnectListener(AplikasiChat aplikasiChat){
            this.aplikasiChat = aplikasiChat;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
             aplikasiChat.connect();
        }
    }

    class TombolKirimListener implements ActionListener{

        private AplikasiChat aplikasiChat;

        public TombolKirimListener(AplikasiChat aplikasiChat){
            this.aplikasiChat = aplikasiChat;
        }

        public void actionPerformed(ActionEvent e) {
            aplikasiChat.kirimChat();
        }

    }

    public static void main(String[] args) {
        AplikasiChat chatApp = new AplikasiChat();
        chatApp.start();
    }

    
}
